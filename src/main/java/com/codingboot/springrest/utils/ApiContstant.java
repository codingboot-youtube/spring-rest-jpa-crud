package com.codingboot.springrest.utils;

public class ApiContstant {

    public static final String CUSTOMER_NOT_FOUND = "Customer not found";
    public static final String SUCCESS = "success";
    public static final String RECORD_DELETED = "Record Deleted";
    public static final String ERROR = "error";
    public static final String CUSTOMER_ALREADY_EXISTS = "Customer already exists";
}
