package com.codingboot.springrest.controller;

import com.codingboot.springrest.CustomerRepository;
import com.codingboot.springrest.entity.Customer;
import com.codingboot.springrest.exceptions.CustomerNotFoundException;
import com.codingboot.springrest.exceptions.DuplicateCustomerException;
import com.codingboot.springrest.pojo.ApiResponse;
import com.codingboot.springrest.utils.ApiContstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;
    @GetMapping("/customer/{id}")
    public ApiResponse<Customer> getCustomer(@PathVariable int id){
        Customer customer = customerRepository.findById(id).orElseThrow(()->new CustomerNotFoundException(ApiContstant.CUSTOMER_NOT_FOUND));
        return  ApiResponse.<Customer>builder().data(customer).status(ApiContstant.SUCCESS).timestamp(LocalDateTime.now()).build();
    }
    @GetMapping("/customer")
    public ApiResponse<List<Customer>> getAllCustomer(){
        return  ApiResponse.<List<Customer>>builder().data(customerRepository.findAll()).status(ApiContstant.SUCCESS).timestamp(LocalDateTime.now()).build();
    }

    @PostMapping("/customer")
    public  ApiResponse<Customer> createNewCustomer(@RequestBody  Customer customer){

        if(customerRepository.findById(customer.getCustomerId()).isPresent()){
            throw new DuplicateCustomerException(ApiContstant.CUSTOMER_ALREADY_EXISTS);
        }
        return  ApiResponse.<Customer>builder().data(customerRepository.save(customer)).status(ApiContstant.SUCCESS).timestamp(LocalDateTime.now()).build();
    }
    @PatchMapping("/customer")
    public  ApiResponse<Customer>  updateCustomer(@RequestBody  Customer customer){
        return  ApiResponse.<Customer>builder().data(customerRepository.save(customer)).status(ApiContstant.SUCCESS).timestamp(LocalDateTime.now()).build();
    }
    @DeleteMapping("/customer/{id}")
    public ApiResponse<String> deleteCustomer(@PathVariable int id){

        Customer customer = customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException(ApiContstant.CUSTOMER_NOT_FOUND));
        customerRepository.delete(customer);

        return ApiResponse.<String>builder().data(ApiContstant.RECORD_DELETED).timestamp(LocalDateTime.now()).status(ApiContstant.SUCCESS).build();
    }
}
