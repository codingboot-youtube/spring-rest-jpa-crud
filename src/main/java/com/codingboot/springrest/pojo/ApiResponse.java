package com.codingboot.springrest.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
@Data
public class ApiResponse<T> {

    private  String status;
    private T data;

    private LocalDateTime timestamp;
}
