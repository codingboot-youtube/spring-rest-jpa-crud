package com.codingboot.springrest.exceptions;

import com.codingboot.springrest.exceptions.CustomerNotFoundException;
import com.codingboot.springrest.pojo.ApiResponse;
import com.codingboot.springrest.utils.ApiContstant;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class APIExceptionHandler {

    @ExceptionHandler(CustomerNotFoundException.class)
    public ApiResponse<String> handleCustomerNotFoundException(CustomerNotFoundException exception){
        return  ApiResponse.<String>builder().data(ApiContstant.CUSTOMER_NOT_FOUND).status(ApiContstant.ERROR).timestamp(LocalDateTime.now()).build();
    }
    @ExceptionHandler(DuplicateCustomerException.class)
    public ApiResponse<String> handleDuplicateCustomerException(DuplicateCustomerException exception){
        return  ApiResponse.<String>builder().data(ApiContstant.CUSTOMER_ALREADY_EXISTS).status(ApiContstant.ERROR).timestamp(LocalDateTime.now()).build();
    }
}
