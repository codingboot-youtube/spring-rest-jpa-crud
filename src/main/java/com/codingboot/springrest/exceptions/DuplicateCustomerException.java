package com.codingboot.springrest.exceptions;

public class DuplicateCustomerException extends  RuntimeException{
    public DuplicateCustomerException(String message){
        super(message);
    }
}
